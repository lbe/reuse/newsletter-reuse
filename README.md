# Newsletter "reuse"
Open source, open data, open science, open hardware... Cette revue de presse généraliste et mensuelle a pour but de sensibiliser le grand public sur les bénéfices de l'open source dans de nombreux domaines.
Vous devez vous inscrire si vous souhaitez recevoir la lettre. Pour l'inscription (ou la désinscription) et la consultation des archives c'est ici : https://groupes.renater.fr/sympa/arc/reuse
Vous pouvez également retrouver toutes les lettres sur GitLab

Jérôme Harmand - Institut national de recherche pour l'agriculture, l'alimentation et l'environnement (INRAE)

Cette lettre est publiée sous la licence Etalab, https://www.etalab.gouv.fr/licence-ouverte-open-licence/ (=CC BY 4.0)
